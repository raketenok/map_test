//
//  EFGeometricsDB+CoreDataProperties.m
//  MAP_TEST
//
//  Created by Yevgen Yefimenko on 01.07.16.
//  Copyright © 2016 Yevgen Yefimenko. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "EFGeometricsDB+CoreDataProperties.h"

@implementation EFGeometricsDB (CoreDataProperties)

@dynamic arrayOfGeo;

@end
