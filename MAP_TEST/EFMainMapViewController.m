//
//  EFMainMapViewController.m
//  MapTest
//
//  Created by Yevgen Yefimenko on 30.06.16.
//  Copyright © 2016 Yevgen Yefimenko. All rights reserved.
//

#import "EFMainMapViewController.h"
#import "EFDataManager.h"
#import "EFGeometricsDB.h"


@import GoogleMaps;

@interface EFMainMapViewController () <CLLocationManagerDelegate,GMSMapViewDelegate>

@end

@implementation EFMainMapViewController {
    GMSMapView *mapView_;
    CGFloat latitudePosition;
    CGFloat longitudePosition;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.arrayWithAllGeos = [[NSMutableArray alloc] init];
   
    [self setupMapView];
    [self setPolygon];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)setupMapView {
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        [self.locationManager requestWhenInUseAuthorization];
    
    [self.locationManager startUpdatingLocation];
    [self.locationManager requestAlwaysAuthorization];
    
    [self performSelector:@selector(showLocation) withObject:nil afterDelay:2];
    
    self.currentMapView.delegate = self;
    
    self.currentMapView.accessibilityElementsHidden = NO;
        
    self.currentMapView.settings.myLocationButton = YES;
    
    self.currentMapView.settings.indoorPicker = NO;
    
    self.currentMapView.mapType = kGMSTypeSatellite;
    
    self.currentMapView.myLocationEnabled = YES;
    
}

-(void) setPolygon{

    NSMutableArray *arrayGeoDB = [[EFDataManager sharedManager]allGeoArray];
    for (EFGeometricsDB *geoDB in arrayGeoDB) {
        NSData *dataImage = [NSData dataWithData:geoDB.arrayOfGeo];
        NSArray *arrTemp = [NSKeyedUnarchiver unarchiveObjectWithData:dataImage];
        NSMutableArray *arrayGeometricsTemp = [NSMutableArray arrayWithArray: arrTemp];
        GMSMutablePath *rect = [GMSMutablePath path];

        for (NSArray *arrayFirst in arrayGeometricsTemp) {
            NSArray *arrayFirstLevel = [NSArray arrayWithArray:arrayFirst];
            
            for (NSArray *arraySecond in arrayFirstLevel) {
                NSArray *arraySecondLevel = [NSArray arrayWithArray:arraySecond];
                
                for (NSArray *arrayThird  in arraySecondLevel) {
                    NSArray *arrayFourthLevel = [NSArray arrayWithArray:arrayThird];
                    
                    for (id object in arrayFourthLevel) {
                        NSLog(@"OBJECT %@",object);
                        
                        NSString *latitudeString = [arrayFourthLevel objectAtIndex:0];
                        CGFloat latitude = latitudeString.floatValue;
                        latitudePosition = latitude;
                        NSString *longitudeString = [arrayFourthLevel objectAtIndex:1];
                        CGFloat longitude = longitudeString.floatValue;
                        longitudePosition = longitude;
                        [rect addCoordinate:CLLocationCoordinate2DMake(longitude, latitude)];
                    }
                }
            }
        }
        GMSPolygon *polygon = [GMSPolygon polygonWithPath:rect];
        polygon.fillColor = [self randomColor];
        polygon.geodesic = YES;
        polygon.strokeColor = [UIColor blackColor];
        polygon.strokeWidth = 2;
        polygon.map = self.currentMapView;
}
    
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:longitudePosition longitude:latitudePosition zoom:11.6];
    mapView_ = [GMSMapView mapWithFrame:self.view.bounds camera:camera];
    
    self.currentMapView.camera = camera;
    
}

- (NSString *)deviceLocation{
    
    NSString *theLocation = [NSString stringWithFormat:@"latitude: %f longitude: %f", self.locationManager.location.coordinate.latitude, self.locationManager.location.coordinate.longitude];
    
    return theLocation;
}

-(void)showLocation{
    NSLog(@"------------%@",[self deviceLocation]);
    
}

-(UIColor*) randomColor {
    CGFloat r = (CGFloat)(arc4random()%256) /255;
    CGFloat g = (CGFloat)(arc4random()%256) /255;
    CGFloat b = (CGFloat)(arc4random()%256) /255;
    return [UIColor colorWithRed:r green:g blue:b alpha:1];
}



@end
