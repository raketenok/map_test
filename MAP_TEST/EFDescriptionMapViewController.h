//

//  EFDescriptionMapViewController.h
//  MAP_TEST
//
//  Created by Yevgen Yefimenko on 30.06.16.
//  Copyright © 2016 Yevgen Yefimenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MKAnnotationView.h>
#import <MapKit/MapKit.h>


#import <GoogleMaps/GoogleMaps.h>

@interface EFDescriptionMapViewController : UIViewController <CLLocationManagerDelegate,GMSMapViewDelegate>


@property (strong, nonatomic) IBOutlet GMSMapView *currentMapView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *cropLabel;
@property (weak, nonatomic) IBOutlet UILabel *areaLabel;

@property (strong, nonatomic) NSString *areaText;
@property (strong, nonatomic) NSString *cropText;
@property (strong, nonatomic) NSString *titleText;
@property (strong, nonatomic) NSMutableArray *arrayOfCoordinates;

@property (retain, nonatomic) CLLocationManager *locationManager;
@property (assign, nonatomic) CLLocationCoordinate2D currentPosition;



@end
