//
//  EFMainViewController.h
//  MapTest
//
//  Created by Yevgen Yefimenko on 30.06.16.
//  Copyright © 2016 Yevgen Yefimenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EFMainMapViewController.h"
#import "EFWealdsTableViewController.h"

@interface EFMainViewController : UIViewController
- (IBAction)changeVcAction:(UISegmentedControl *)sender;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedProperty;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic)  UIViewController *currentViewController;


@end
