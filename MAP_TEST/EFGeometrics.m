//
//  EFGeometrics.m
//  MAP_TEST
//
//  Created by Yevgen Yefimenko on 01.07.16.
//  Copyright © 2016 Yevgen Yefimenko. All rights reserved.
//

#import "EFGeometrics.h"

@implementation EFGeometrics


-(instancetype)initWithResponse:(NSDictionary*) dictionary{
    self = [super init];
    
    if (self) {
        
        self.arrayOfCoordinates = [[NSArray alloc]init];
        self.arrayOfCoordinates = [dictionary objectForKey:@"coordinates"];

    }
    return self;
}

@end
