//
//  EFGeometricsDB+CoreDataProperties.h
//  MAP_TEST
//
//  Created by Yevgen Yefimenko on 01.07.16.
//  Copyright © 2016 Yevgen Yefimenko. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "EFGeometricsDB.h"

NS_ASSUME_NONNULL_BEGIN

@interface EFGeometricsDB (CoreDataProperties)

@property (nullable, nonatomic, retain) NSData *arrayOfGeo;

@end

NS_ASSUME_NONNULL_END
