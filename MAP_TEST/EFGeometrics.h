//
//  EFGeometrics.h
//  MAP_TEST
//
//  Created by Yevgen Yefimenko on 01.07.16.
//  Copyright © 2016 Yevgen Yefimenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EFGeometrics : NSObject


-(instancetype)initWithResponse:(NSDictionary*) dictionary;

@property(strong,nonatomic) NSArray *arrayOfCoordinates;


@end
