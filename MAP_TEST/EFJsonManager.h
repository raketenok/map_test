//
//  EFJsonManager.h
//  MapTest
//
//  Created by Yevgen Yefimenko on 30.06.16.
//  Copyright © 2016 Yevgen Yefimenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EFJsonManager : NSObject


@property(strong,nonatomic) NSMutableArray *arrayItems;
@property(strong,nonatomic) NSMutableArray *arrayGeometrics;




+(EFJsonManager*) sharedManager;

-(NSMutableArray*) responseJsonForItems;
-(NSMutableArray*) responseJsonForGeo;



@end
