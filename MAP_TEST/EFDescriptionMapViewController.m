//
//  EFDescriptionMapViewController.m
//  MapTest
//
//  Created by Yevgen Yefimenko on 30.06.16.
//  Copyright © 2016 Yevgen Yefimenko. All rights reserved.
//

#import "EFDescriptionMapViewController.h"

@interface EFDescriptionMapViewController ()

@end


@implementation EFDescriptionMapViewController{
    
    GMSMapView *mapView_;
    CGFloat latitudePosition;
    CGFloat longitudePosition;
}


- (void)viewDidLoad {
    [super viewDidLoad];
 
    [self setupMapView];
    [self loadSettings];
    [self setPolygon];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void) loadSettings {
    
    self.navigationItem.title = self.titleText;
    self.areaLabel.text = [NSString stringWithFormat:@"%@ ha",self.areaText];
    self.cropLabel.text = self.cropText;
    self.titleLabel.text = self.titleText;
   
}


- (void)setupMapView {
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        [self.locationManager requestWhenInUseAuthorization];
    
    [self.locationManager startUpdatingLocation];
    [self.locationManager requestAlwaysAuthorization];
    
    [self performSelector:@selector(showLocation) withObject:nil afterDelay:2];
    
    self.currentMapView.delegate = self;
    
    self.currentMapView.accessibilityElementsHidden = NO;
        
    self.currentMapView.settings.myLocationButton = YES;
    
    self.currentMapView.settings.indoorPicker = NO;
    
    self.currentMapView.mapType = kGMSTypeSatellite;
    
    self.currentMapView.myLocationEnabled = YES;
    
    
}

-(void) setPolygon{
    
    GMSMutablePath *rect = [GMSMutablePath path];
    
    for (NSArray *arrayFirst in self.arrayOfCoordinates) {
        NSArray *arrayFirstLevel = [NSArray arrayWithArray:arrayFirst];
        
        for (NSArray *arraySecond in arrayFirstLevel) {
            NSArray *arraySecondLevel = [NSArray arrayWithArray:arraySecond];
            
            for (NSArray *arrayThird  in arraySecondLevel) {
                NSArray *arrayFourthLevel = [NSArray arrayWithArray:arrayThird];
                
                for (id object in arrayFourthLevel) {
                    NSLog(@"OBJECT %@",object);
                    
                    NSString *longitudeString = [arrayFourthLevel objectAtIndex:0];
                    CGFloat longitude = longitudeString.floatValue;
                    longitudePosition = longitude;
                    NSString *latitudeString = [arrayFourthLevel objectAtIndex:1];
                    CGFloat latitude = latitudeString.floatValue;
                    latitudePosition = latitude;
                    [rect addCoordinate:CLLocationCoordinate2DMake(latitude, longitude)];
                    
                }
            }
        }
        
    }
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:latitudePosition longitude:longitudePosition zoom:13];
    mapView_ = [GMSMapView mapWithFrame:self.view.bounds camera:camera];
    
    self.currentMapView.camera = camera;
    GMSPolygon *polygon = [GMSPolygon polygonWithPath:rect];
    polygon.fillColor = [UIColor lightGrayColor];
    polygon.geodesic = YES;
    polygon.strokeColor = [UIColor blackColor];
    polygon.strokeWidth = 2;
    polygon.map = self.currentMapView;
}

- (NSString *)deviceLocation{
    
    NSString *theLocation = [NSString stringWithFormat:@"latitude: %f longitude: %f", self.locationManager.location.coordinate.latitude, self.locationManager.location.coordinate.longitude];

    return theLocation;
}

-(void)showLocation{
    NSLog(@"------------%@",[self deviceLocation]);
    
}



@end
