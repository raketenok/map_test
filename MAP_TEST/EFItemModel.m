//
//  EFItemModel.m
//  MAP_TEST
//
//  Created by Yevgen Yefimenko on 30.06.16.
//  Copyright © 2016 Yevgen Yefimenko. All rights reserved.
//

#import "EFItemModel.h"

@implementation EFItemModel

-(instancetype)initWithResponse:(NSDictionary*) dictionary{
    self = [super init];
    
    if (self) {
    
    self.itemTitle = [dictionary objectForKey:@"name"];
    self.itemArea = [[dictionary objectForKey:@"till_area"]stringValue];
    self.itemCrop = [dictionary objectForKey:@"crop"];

    }
    return self;
}

@end
