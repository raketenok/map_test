//
//  EFWealdsTableViewController.h
//  MapTest
//
//  Created by Yevgen Yefimenko on 30.06.16.
//  Copyright © 2016 Yevgen Yefimenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EFMainMapViewController.h"
#import "EFWealdTableViewCell.h"
#import "EFJsonManager.h"



@interface EFWealdsTableViewController : UITableViewController

@property(strong,nonatomic) NSArray *arrayItems;
@property(strong,nonatomic) NSMutableArray *arrayGeometrics;
@property(strong,nonatomic) NSMutableArray *arrayGeometricsTemp;
@property(strong,nonatomic) NSMutableArray *arrayGeoOfItem;


@end
