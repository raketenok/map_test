//
//  EFMainMapViewController.h
//  MAP_TEST
//
//  Created by Yevgen Yefimenko on 30.06.16.
//  Copyright © 2016 Yevgen Yefimenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <GoogleMaps/GoogleMaps.h>

@interface EFMainMapViewController : UIViewController

@property (retain, nonatomic) CLLocationManager *locationManager;
@property (strong,nonatomic) NSMutableArray *arrayWithAllGeos;
@property (weak, nonatomic) IBOutlet GMSMapView *currentMapView;


@end
