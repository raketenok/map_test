//
//  EFWealdsTableViewController.m
//  MapTest
//
//  Created by Yevgen Yefimenko on 30.06.16.
//  Copyright © 2016 Yevgen Yefimenko. All rights reserved.
//

#import "EFWealdsTableViewController.h"
#import "EFDescriptionMapViewController.h"
#import "EFDataManager.h"
#import "EFItemDB.h"
#import "EFGeometricsDB.h"

@interface EFWealdsTableViewController ()

@property (strong, nonatomic) EFItemDB *item;
@property (strong, nonatomic) EFGeometricsDB *geometricsDB;



@end


@implementation EFWealdsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.arrayGeometricsTemp = [[NSMutableArray alloc]init];
    self.arrayGeoOfItem = [[NSMutableArray alloc]init];
   self.arrayItems = [[EFJsonManager sharedManager]responseJsonForItems];
   self.arrayGeometrics = [[EFJsonManager sharedManager] responseJsonForGeo];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated{
    
    NSArray *arrayItemsDB = [[EFDataManager sharedManager]allItemsArray];
    self.arrayItems = arrayItemsDB;
    NSMutableArray *arrayGeoDB = [[EFDataManager sharedManager]allGeoArray];
    self.arrayGeometrics = arrayGeoDB;
    
    for (EFGeometricsDB *geoDB in arrayGeoDB) {
        NSData *dataImage = [NSData dataWithData:geoDB.arrayOfGeo];
        NSArray *arrTemp = [NSKeyedUnarchiver unarchiveObjectWithData:dataImage];
        NSMutableArray *arrayGeometricsTemp = [NSMutableArray arrayWithArray: arrTemp];

        [self.arrayGeometricsTemp addObject:arrayGeometricsTemp];
    }
    

}
#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.arrayItems count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"EFWealdTableViewCell";
    EFWealdTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    EFItemDB *item = [self.arrayItems objectAtIndex:indexPath.row];
    
    cell.title.text =  item.title;
    cell.area.text = [NSString stringWithFormat:@"%@ ha",item.area];
    cell.crop.text = item.crop;
    
    return cell;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    self.item = [self.arrayItems objectAtIndex:indexPath.row];
    EFGeometricsDB *geo = [self.arrayGeometrics objectAtIndex:indexPath.row];
    NSData *dataImage = [NSData dataWithData:geo.arrayOfGeo];
    NSArray *arrTemp = [NSKeyedUnarchiver unarchiveObjectWithData:dataImage];
    self.arrayGeoOfItem = [NSMutableArray arrayWithArray: arrTemp];
    
    [self performSegueWithIdentifier:@"toDetailViewController" sender:nil];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    EFDescriptionMapViewController *vc = [[EFDescriptionMapViewController alloc]init];
    vc = segue.destinationViewController;
    
    vc.titleText = self.item.title;
    vc.areaText = self.item.area;
    vc.cropText = self.item.crop;
    vc.arrayOfCoordinates = [[NSMutableArray alloc]initWithArray:self.arrayGeoOfItem];

}


@end
