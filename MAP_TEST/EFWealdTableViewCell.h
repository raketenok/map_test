//
//  EFWealdTableViewCell.h
//  MapTest
//
//  Created by Yevgen Yefimenko on 30.06.16.
//  Copyright © 2016 Yevgen Yefimenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EFWealdTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *crop;
@property (weak, nonatomic) IBOutlet UILabel *area;

@end
