//
//  main.m
//  MAP_TEST
//
//  Created by Yevgen Yefimenko on 30.06.16.
//  Copyright © 2016 Yevgen Yefimenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char *argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
