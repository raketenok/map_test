//
//  EFMainViewController.m
//  MapTest
//
//  Created by Yevgen Yefimenko on 30.06.16.
//  Copyright © 2016 Yevgen Yefimenko. All rights reserved.
//

#import "EFMainViewController.h"

@interface EFMainViewController ()

@end

@implementation EFMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    UIViewController *vc = [self viewControllerForSegmentIndex:self.segmentedProperty.selectedSegmentIndex];
    [self addChildViewController:vc];
    vc.view.frame = self.contentView.bounds;
    [self.contentView addSubview:vc.view];
    self.currentViewController = vc;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)changeVcAction:(UISegmentedControl *)sender {
    UIViewController *vc = [self viewControllerForSegmentIndex:sender.selectedSegmentIndex];

    [self addChildViewController:vc];
    [self transitionFromViewController:self.currentViewController toViewController:vc duration:0.5 options:UIViewAnimationOptionLayoutSubviews animations:^{
        [self.currentViewController.view removeFromSuperview];
        vc.view.frame = self.contentView.bounds;
        [self.contentView addSubview:vc.view];
    } completion:nil];
    
}

- (UIViewController *)viewControllerForSegmentIndex:(NSInteger)index {
    UIViewController *vc;
    
    switch (index) {
        case 0:
            vc =  [self.storyboard instantiateViewControllerWithIdentifier:@"EFWealdsTableViewController"];
            
            break;
        case 1:
            vc = [self.storyboard instantiateViewControllerWithIdentifier:@"EFMainMapViewController"];
            
            break;
    }
    return vc;
}

@end
