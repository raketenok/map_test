//
//  EFJsonManager.m
//  MapTest
//
//  Created by Yevgen Yefimenko on 30.06.16.
//  Copyright © 2016 Yevgen Yefimenko. All rights reserved.
//

#import "EFJsonManager.h"
#import "EFItemModel.h"
#import "EFGeometrics.h"
#import "EFDataManager.h"
#import "EFItemDB.h"
#import "EFGeometricsDB.h"


@interface EFJsonManager ()

@property(strong,nonatomic) EFItemDB *itemDB;


@end

@implementation EFJsonManager

+(EFJsonManager*) sharedManager{
    
    static EFJsonManager *manager  = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        manager = [[EFJsonManager alloc] init];
    });
    
    return manager;
}


#pragma mark - Response JSON

-(NSMutableArray*) responseJsonForItems{
    
    [[EFDataManager sharedManager]deleteAllItems];
    NSArray *dictArray = [self responseJsonArray];

    self.arrayItems = [[NSMutableArray alloc]init];
    for (NSDictionary *dict in dictArray) {
        NSDictionary *diction = [dict objectForKey:@"properties"];
        EFItemModel *item  = [[EFItemModel alloc] initWithResponse:diction];

            EFItemDB *itemDB = [NSEntityDescription insertNewObjectForEntityForName:@"EFItemDB" inManagedObjectContext:[[EFDataManager sharedManager]managedObjectContext]];
            itemDB.title = item.itemTitle;
            itemDB.area = item.itemArea;
            itemDB.crop = item.itemCrop;
            
            [self.arrayItems addObject:itemDB];
            [[[EFDataManager sharedManager]managedObjectContext]save:nil];
        
       }
    
    return self.arrayItems;
}

-(NSMutableArray*) responseJsonForGeo{

    [[EFDataManager sharedManager]deleteAllGeo];

    NSArray *dictArray = [self responseJsonArray];
    self.arrayGeometrics = [[NSMutableArray alloc]init];
    
    for (NSDictionary *dict in dictArray) {
        NSDictionary *diction = [dict objectForKey:@"geometry"];
        
        EFGeometrics *item  = [[EFGeometrics alloc] initWithResponse:diction];
        
        EFGeometricsDB *itemDB = [NSEntityDescription insertNewObjectForEntityForName:@"EFGeometricsDB" inManagedObjectContext:[[EFDataManager sharedManager]managedObjectContext]];
        
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:item.arrayOfCoordinates];
        itemDB.arrayOfGeo = data;

        [self.arrayGeometrics addObject:item];
        [[[EFDataManager sharedManager]managedObjectContext]save:nil];

    }
    
    return self.arrayGeometrics;
}

-(NSArray*) responseJsonArray{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"fields" ofType:@"json"];
    NSString *myJSON = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:NULL];
    
    NSData *data = [myJSON dataUsingEncoding:NSUTF8StringEncoding];
    NSError *error;
    NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                   options:0
                                                                     error:&error];
    
    NSArray *dictArray = [jsonDictionary objectForKey:@"features"];
    if (error)
    {
        NSLog(@"Error al leer json: %@", [error description]);
    }
    return dictArray;
}

@end
