//
//  EFGeometricsDB.h
//  MAP_TEST
//
//  Created by Yevgen Yefimenko on 01.07.16.
//  Copyright © 2016 Yevgen Yefimenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface EFGeometricsDB : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "EFGeometricsDB+CoreDataProperties.h"
