//
//  EFItemModel.h
//  MAP_TEST
//
//  Created by Yevgen Yefimenko on 30.06.16.
//  Copyright © 2016 Yevgen Yefimenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EFItemModel : NSObject

@property(strong,nonatomic) NSString *itemTitle;
@property(strong,nonatomic) NSString *itemArea;
@property(strong,nonatomic) NSString *itemCrop;

-(instancetype)initWithResponse:(NSDictionary*) dictionary;

@end
